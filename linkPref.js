jQuery(document).ready(function($) {

  function linkopener(a) {
    $('.s-lib-box-content a').each(function() { // all links within boxes will be affected. Links in header/footer will be ignored
      if ($(this).is('[href*="researchguides.scc.losrios.edu"]')) {} else {
        $(this).attr('target', a); 
      }
    });
  }

  function setCookie(cname, cvalue, exdays, target) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + '; ' + expires + '; path=/';
    checkCookie(); // this allows new event listener to be added

  }

  function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1);
      if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return '';
  }

  function checkCookie() {
    var preference = getCookie('windowPref');
    var winPref = $('#target-setting'); // need to have checkbox with ID target-setting
    if (preference === 'new') { // if cookie has been set as wanting links to open in new window
      $(winPref).prop('checked', true); // show box as checked to represent current state
      $(winPref).on('click', function() {
        setCookie('windowPref', 'same', 30, '_self');
      }); // next time someone checks it, will reset cookie
      setTimeout(function() {
        linkopener('_blank');
      }, 800); // delay is necessary because Libguides content loads dynamically

    } else { // if cookie is set to same, or if it does not exist
      $(winPref).prop('checked', false);
      $(winPref).on('click', function() {
        setCookie('windowPref', 'new', 30, '_blank');
      }); // click will set cookie
      linkopener('_self');
    }
  }
  checkCookie();
});