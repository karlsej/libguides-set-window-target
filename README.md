Link Target Switcher for Libguides
==================================
People argue about whether links should open in new windows. This script lets the user choose. It will change the target for links in Libguides boxes when a user clicks a checkbox. It sets a cookie on the local machine to remember the preference.

Use
---

Change the domain string in the **linkopener** function to your libguides domain, so that e.g. links to other libguides will not be affected. If this is not important, simply delete the **if** statement that includes the domain string.

Create a checkbox somewhere in your template, e.g.

    <input id="target-setting" type="checkbox"> <label for="target-setting"> Open links in new windows/tabs</label>
Put the checkbox somewhere on the page, e.g. top right.
Include winPref.js in the page.

Note
---
Because Libguides currently masks database names on the A-to-Z page and on Subject pages, those links appear to be on the local domain, so those link targets will not be changed. (Enhancement needed.)
Cookie functions are adapted from [W3Schools Cookies Tutorial](http://www.w3schools.com/js/js_cookies.asp), and the linkopener function is adapted from [Smashing Magazine](http://www.smashingmagazine.com/2008/07/01/should-links-open-in-new-windows/).
